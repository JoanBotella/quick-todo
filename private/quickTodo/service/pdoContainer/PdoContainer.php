<?php
declare(strict_types=1);

namespace quickTodo\service\pdoContainer;

use PDO;
use quickTodo\service\configuration\Configuration;

final class PdoContainer
{

	private static PDO $pdo;

	public static function get():PDO
	{
		if (!isset(self::$pdo))
		{
			self::$pdo = self::buildPdo();
		}
		return self::$pdo;
	}

		private static function buildPdo():PDO
		{
			$dsn = 'sqlite:'.Configuration::getDbFilePath();
			return new PDO($dsn);
		}

}
