<?php
declare(strict_types=1);

namespace quickTodo\service\configuration;

abstract class ConfigurationAbs
{

	abstract public static function getBaseRequestUri():string;

	abstract public static function getDbFilePath():string;

}