<?php
declare(strict_types=1);

namespace quickTodo\service\repository\todoList;

use quickTodo\service\pdoContainer\PdoContainer;
use quickTodo\service\repository\todoItem\TodoItemRepository;
use quickTodo\library\repository\RepositoryAbs;

final class TodoListRepository extends RepositoryAbs
{

	public static function selectAll():array
	{
		$pdo = PdoContainer::get();
		$sql = '
			SELECT
				*
			FROM
				`todo_list`
			ORDER BY
				`name`
		';
		return $pdo->query($sql)->fetchAll();
	}

	public static function selectById(int $id):array
	{
		$pdo = PdoContainer::get();
		$sql = '
			SELECT
				*
			FROM
				`todo_list`
			WHERE
				`id` = '.$id.'
		';
		return $pdo->query($sql)->fetch();
	}

	public static function insert(string $name):void
	{
		$pdo = PdoContainer::get();
		$sql = '
			INSERT INTO
				`todo_list`
				(
					`name`
				)
			VALUES
				(
					\''.$name.'\'
				)
			;
		';
		$pdo->exec($sql);
	}

	public static function updateById(int $id, string $name):void
	{
		$pdo = PdoContainer::get();
		$sql = '
			UPDATE
				`todo_list`
			SET
				`name` = \''.$name.'\'
			WHERE
				`id` = '.$id.'
		';
		$pdo->exec($sql);
	}

	public static function deleteById(int $id):void
	{
		$pdo = PdoContainer::get();
		$sql = '
			DELETE FROM
				`todo_list`
			WHERE
				`id` = '.$id.'
		';
		$pdo->exec($sql);

		TodoItemRepository::deleteByTodoListId($id);
	}

}