<?php
declare(strict_types=1);

namespace quickTodo\service\repository\todoItem;

use quickTodo\service\pdoContainer\PdoContainer;
use quickTodo\library\repository\RepositoryAbs;

final class TodoItemRepository extends RepositoryAbs
{

	public static function selectByTodoListId(int $todoListId):array
	{
		$pdo = PdoContainer::get();
		$sql = '
			SELECT
				*
			FROM
				`todo_item`
			WHERE
				`todo_list_id`='.$todoListId.'
			ORDER BY
				`checked`,
				`caption`
		';

		return $pdo->query($sql)->fetchAll();
	}

	public static function selectById(int $id):array
	{
		$pdo = PdoContainer::get();
		$sql = '
			SELECT
				*
			FROM
				`todo_item`
			WHERE
				`id` = '.$id.'
		';
		return $pdo->query($sql)->fetch();
	}

	public static function insert(int $todoListId, string $caption, bool $checked):void
	{
		$pdo = PdoContainer::get();
		$sql = '
			INSERT INTO
				`todo_item`
				(
					`todo_list_id`,
					`caption`,
					`checked`
				)
			VALUES
				(
					'.$todoListId.',
					\''.$caption.'\',
					'.static::boolToInteger($checked).'
				)
			;
		';
		$pdo->exec($sql);
	}

	public static function updateById(int $id, string $caption, bool $checked):void
	{
		$pdo = PdoContainer::get();
		$sql = '
			UPDATE
				`todo_item`
			SET
				`caption` = \''.$caption.'\',
				`checked` = '.static::boolToInteger($checked).'
			WHERE
				`id` = '.$id.'
			;
		';
		$pdo->exec($sql);
	}

	public static function deleteById(int $id):void
	{
		$pdo = PdoContainer::get();
		$sql = '
			DELETE FROM
				`todo_item`
			WHERE
				`id` = '.$id.'
			;
		';
		$pdo->exec($sql);
	}

	public static function deleteByTodoListId(int $todoListId):void
	{
		$pdo = PdoContainer::get();
		$sql = '
			DELETE FROM
				`todo_item`
			WHERE
				`todo_list_id` = '.$todoListId.'
			;
		';
		$pdo->exec($sql);
	}

}