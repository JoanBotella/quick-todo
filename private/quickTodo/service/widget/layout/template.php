<?php
declare(strict_types=1);

use quickTodo\service\configuration\Configuration;

?><!DOCTYPE html>
<html lang="<?= $context['languageCode'] ?>" data-page="<?= $context['pageCode'] ?>" class="page-<?= $context['pageCode'] ?>">
<head>
	<meta charset="UTF-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

	<base href="<?= Configuration::getBaseRequestUri() ?>" />

	<link rel="icon" href="asset/img/favicon.png" type="image/png" />

	<title><?= $context['pageTitle'] ?> | Quick ToDo</title>

	<link href="asset/css/style.css" rel="stylesheet" media="screen" />
	<link href="asset/fontawesome/css/all.min.css" rel="stylesheet" media="screen" />
</head>
<body>

	<div class="widget-layout">

		<noscript>
			<p>You will need JavaScript to play with me!</p>
		</noscript>

		<header>
			<?= $context['headerContent'] ?>
		</header>

		<nav>
			<?= $context['navContent'] ?>
		</nav>

		<main>
			<?= $context['mainContent'] ?>
		</main>

	</div>

	<script src="asset/js/script.js"></script>
</body>
</html>