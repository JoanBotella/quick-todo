<?php
declare(strict_types=1);

namespace quickTodo\service\widget\layout;

use quickTodo\service\templater\Templater;

final class LayoutWidget
{

	public static function render(array $context = []):string
	{
		return Templater::render(
			__DIR__.'/template.php',
			$context
		);
	}

}