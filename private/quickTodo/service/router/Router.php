<?php
declare(strict_types=1);

namespace quickTodo\service\router;

use Exception;
use quickTodo\page\internalServerError\library\controller\InternalServerErrorController;
use quickTodo\page\notFound\library\controller\NotFoundController;
use quickTodo\library\controller\ControllerItf;
use quickTodo\service\requestPathContainer\RequestPathContainer;

final class Router
{

	public static function route():void
	{
		try
		{
			$controller = self::buildController();
			$controller->action();
		}
		catch (Exception $e)
		{
			$controller = new InternalServerErrorController();
			$controller->action();
		}
	}

		private function buildController():ControllerItf
		{
			$result = preg_match(
				'/^([^?\/]*)/',
				RequestPathContainer::get(),
				$matches
			);

			if ($result !== 1)
			{
				throw new Exception('Page controller could not be guessed');
			}

			$pageSlug = 
				$matches[1] == ''
					? 'home'
					: $matches[1]
			;

			$classNamespace = '\\quickTodo\\page\\'.$pageSlug.'\\library\\controller\\'.ucfirst($pageSlug).'Controller';

			if (class_exists($classNamespace))
			{
				return new $classNamespace();
			}

			return new NotFoundController();
		}

}