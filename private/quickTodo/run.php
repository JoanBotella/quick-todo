<?php
declare(strict_types=1);

require __DIR__.'/setup.php';

use quickTodo\service\router\Router;

Router::route();