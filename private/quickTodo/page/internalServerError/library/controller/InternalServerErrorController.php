<?php
declare(strict_types=1);

namespace quickTodo\page\internalServerError\library\controller;

use quickTodo\library\controller\ControllerAbs;
use quickTodo\page\internalServerError\service\widget\headerContent\InternalServerErrorHeaderContentWidget;
use quickTodo\page\internalServerError\service\widget\mainContent\InternalServerErrorMainContentWidget;
use quickTodo\page\internalServerError\service\widget\navContent\InternalServerErrorNavContentWidget;
use quickTodo\service\widget\layout\LayoutWidget;

final class InternalServerErrorController extends ControllerAbs
{

	protected function respond():void
	{
		http_response_code(500);

		$context = [
			'languageCode' => 'en',
			'pageCode' => 'internal_server_error',
			'pageTitle' => 'Internal Server Error',
		];

		$context['headerContent'] = InternalServerErrorHeaderContentWidget::render($context);

		$context['navContent'] = InternalServerErrorNavContentWidget::render($context);

		$context['mainContent'] = InternalServerErrorMainContentWidget::render($context);

		echo LayoutWidget::render($context);
	}

}