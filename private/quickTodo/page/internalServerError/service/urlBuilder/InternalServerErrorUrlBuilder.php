<?php
declare(strict_types=1);

namespace quickTodo\page\internalServerError\service\urlBuilder;

final class InternalServerErrorUrlBuilder
{

	public static function build():string
	{
		return 'internalServerError';
	}

}