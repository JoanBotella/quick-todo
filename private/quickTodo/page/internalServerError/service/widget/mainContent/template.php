<?php
declare(strict_types=1);

?>

<div class="widget-internal_server_error-main_content">

	<p>Something crashed on the server side!</p>

</div>
