<?php
declare(strict_types=1);

namespace quickTodo\page\internalServerError\service\widget\mainContent;

use quickTodo\service\templater\Templater;

final class InternalServerErrorMainContentWidget
{

	public static function render(array $context):string
	{
		return Templater::render(
			__DIR__.'/template.php',
			$context
		);
	}

}