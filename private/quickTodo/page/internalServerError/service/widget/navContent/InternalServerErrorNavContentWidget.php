<?php
declare(strict_types=1);

namespace quickTodo\page\internalServerError\service\widget\navContent;

use quickTodo\service\templater\Templater;

final class InternalServerErrorNavContentWidget
{

	public static function render(array $context):string
	{
		return Templater::render(
			__DIR__.'/template.php',
			$context
		);
	}

}