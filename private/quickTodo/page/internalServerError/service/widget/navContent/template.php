<?php
declare(strict_types=1);

use quickTodo\page\home\service\urlBuilder\HomeUrlBuilder;

?>

<ul class="widget-internal_server_error-nav_content">
	<li class="back"><a class="fas fa-caret-square-left" href="<?= HomeUrlBuilder::build() ?>"></a></li>
</ul>
