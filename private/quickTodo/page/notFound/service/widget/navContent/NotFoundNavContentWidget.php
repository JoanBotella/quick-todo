<?php
declare(strict_types=1);

namespace quickTodo\page\notFound\service\widget\navContent;

use quickTodo\service\templater\Templater;

final class NotFoundNavContentWidget
{

	public static function render(array $context):string
	{
		return Templater::render(
			__DIR__.'/template.php',
			$context
		);
	}

}