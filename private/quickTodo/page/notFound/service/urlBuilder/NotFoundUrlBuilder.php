<?php
declare(strict_types=1);

namespace quickTodo\page\notFound\service\urlBuilder;

final class NotFoundUrlBuilder
{

	public static function build():string
	{
		return 'notFound';
	}

}