<?php
declare(strict_types=1);

namespace quickTodo\page\notFound\library\controller;

use quickTodo\library\controller\ControllerAbs;
use quickTodo\page\notFound\service\widget\headerContent\NotFoundHeaderContentWidget;
use quickTodo\page\notFound\service\widget\mainContent\NotFoundMainContentWidget;
use quickTodo\page\notFound\service\widget\navContent\NotFoundNavContentWidget;
use quickTodo\service\widget\layout\LayoutWidget;

final class NotFoundController extends ControllerAbs
{

	protected function respond():void
	{
		http_response_code(404);

		$context = [
			'languageCode' => 'en',
			'pageCode' => 'not_found',
			'pageTitle' => 'Not Found',
		];

		$context['headerContent'] = NotFoundHeaderContentWidget::render($context);

		$context['navContent'] = NotFoundNavContentWidget::render($context);

		$context['mainContent'] = NotFoundMainContentWidget::render($context);

		echo LayoutWidget::render($context);
	}

}