<?php
declare(strict_types=1);

namespace quickTodo\page\processTodoItemInsert\library\controller;

use quickTodo\library\controller\ControllerAbs;
use quickTodo\page\todoList\service\urlBuilder\TodoListUrlBuilder;
use quickTodo\service\repository\todoItem\TodoItemRepository;

final class ProcessTodoItemInsertController extends ControllerAbs
{

	private int $todoListId;

	private string $caption;

	private bool $checked;

	protected function setupAndValidateInput():void
	{
		$this->todoListId = (int) $_POST['todo_list_id'];
		$this->caption = $_POST['caption'];
		$this->checked = isset($_POST['checked']);
	}

	protected function run():void
	{
		TodoItemRepository::insert(
			$this->todoListId,
			$this->caption,
			$this->checked
		);
	}

	protected function respond():void
	{
		header('location: '.TodoListUrlBuilder::build($this->todoListId));
	}

}