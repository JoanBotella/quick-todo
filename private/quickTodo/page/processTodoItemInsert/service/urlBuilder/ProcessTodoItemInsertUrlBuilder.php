<?php
declare(strict_types=1);

namespace quickTodo\page\processTodoItemInsert\service\urlBuilder;

final class ProcessTodoItemInsertUrlBuilder
{

	public static function build():string
	{
		return 'processTodoItemInsert';
	}

}