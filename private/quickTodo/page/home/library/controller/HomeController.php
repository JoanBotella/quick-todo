<?php
declare(strict_types=1);

namespace quickTodo\page\home\library\controller;

use quickTodo\library\controller\ControllerAbs;
use quickTodo\page\home\service\widget\headerContent\HomeHeaderContentWidget;
use quickTodo\page\home\service\widget\mainContent\HomeMainContentWidget;
use quickTodo\page\home\service\widget\navContent\HomeNavContentWidget;
use quickTodo\service\repository\todoList\TodoListRepository;
use quickTodo\service\widget\layout\LayoutWidget;

final class HomeController extends ControllerAbs
{

	protected function respond():void
	{
		$context = [
			'languageCode' => 'en',
			'pageCode' => 'home',
			'pageTitle' => 'ToDo Lists',

			'todoLists' => TodoListRepository::selectAll(),
		];

		$context['headerContent'] = HomeHeaderContentWidget::render($context);

		$context['navContent'] = HomeNavContentWidget::render($context);

		$context['mainContent'] = HomeMainContentWidget::render($context);

		echo LayoutWidget::render($context);
	}

}