<?php
declare(strict_types=1);

use quickTodo\page\processTodoListDelete\service\urlBuilder\ProcessTodoListDeleteUrlBuilder;
use quickTodo\page\todoList\service\urlBuilder\TodoListUrlBuilder;

?>

<ul class="widget-home-main_content">
	<?php foreach ($context['todoLists'] as $todoList): ?>
	<li>
		<a class="delete fas fa-minus-square" href="<?= ProcessTodoListDeleteUrlBuilder::buildByTodoListId((int) $todoList['id']) ?>" data-hook="delete_link"></a>
		<a class="name" href="<?= TodoListUrlBuilder::build((int) $todoList['id']) ?>"><?= $todoList['name'] ?></a>
		<a class="show fas fa-caret-square-right" href="<?= TodoListUrlBuilder::build((int) $todoList['id']) ?>"></a>
	</li>
	<?php endforeach ?>
</ul>
