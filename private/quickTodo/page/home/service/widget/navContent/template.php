<?php
declare(strict_types=1);

use quickTodo\page\todoListForm\service\urlBuilder\TodoListFormUrlBuilder;

?>

<ul class="widget-home-nav_content">
	<li class="add"><a class="fas fa-plus-square" href="<?= TodoListFormUrlBuilder::build() ?>"></a></li>
</ul>
