
pageSetuppers['home'] = function ()
{
	const confirmDeletion = function (event)
	{
		if (!confirm('Are you sure?'))
		{
			event.preventDefault();
		}
	}

	const addConfirmToDeleteLinks = function ()
	{
		const deleteLinks = document.querySelectorAll('[data-hook="delete_link"]');
		for (let i = 0; i < deleteLinks.length; i++)
		{
			deleteLinks[i].addEventListener('click', confirmDeletion);
		}
	}

	addConfirmToDeleteLinks();
}