<?php
declare(strict_types=1);

namespace quickTodo\page\processTodoListDelete\service\urlBuilder;

final class ProcessTodoListDeleteUrlBuilder
{

	public static function buildByTodoListId(int $todoListId):string
	{
		return 'processTodoListDelete?todo_list_id='.$todoListId;
	}

}