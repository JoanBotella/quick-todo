<?php
declare(strict_types=1);

namespace quickTodo\page\processTodoListDelete\library\controller;

use quickTodo\library\controller\ControllerAbs;
use quickTodo\page\home\service\urlBuilder\HomeUrlBuilder;
use quickTodo\service\repository\todoList\TodoListRepository;

final class ProcessTodoListDeleteController extends ControllerAbs
{

	private int $todoListId;

	protected function setupAndValidateInput():void
	{
		$this->todoListId = (int) $_GET['todo_list_id'];
	}

	protected function run():void
	{
		TodoListRepository::deleteById(
			$this->todoListId
		);
	}

	protected function respond():void
	{
		header('location: '.HomeUrlBuilder::build());
	}

}