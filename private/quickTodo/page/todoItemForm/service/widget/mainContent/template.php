<?php
declare(strict_types=1);

?>

<form class="widget-todo_item_form-main_content" action="<?= $context['formAction'] ?>" method="POST">

	<?php if (isset($context['todoList'])): ?>
	<input type="hidden" name="todo_list_id" value="<?= $context['todoList']['id'] ?>" />
	<?php endif ?>

	<?php if (isset($context['todoItem'])): ?>
	<input type="hidden" name="todo_item_id" value="<?= $context['todoItem']['id'] ?>" />
	<?php endif ?>

	<label>
		<span>Caption</span>
		<input name="caption"<?php if (isset($context['todoItem'])): ?> value="<?= $context['todoItem']['caption'] ?>"<?php endif ?> autofocus="autofocus" />
	</label>

	<label>
		<input type="checkbox" name="checked"<?php if (isset($context['todoItem']) && $context['todoItem']['checked'] == '1'): ?> checked="checked"<?php endif ?> />
		<span>Checked</span>
	</label>

	<button>Save</button>

</form>
