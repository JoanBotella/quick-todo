<?php
declare(strict_types=1);

namespace quickTodo\page\todoItemForm\service\widget\navContent;

use quickTodo\service\templater\Templater;

final class TodoItemFormNavContentWidget
{

	public static function render(array $context):string
	{
		return Templater::render(
			__DIR__.'/template.php',
			$context
		);
	}

}