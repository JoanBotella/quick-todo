<?php
declare(strict_types=1);

?>

<ul class="widget-todo_item_form-nav_content">
	<li class="back"><a class="fas fa-caret-square-left" href="<?= $context['todoListUrl'] ?>"></a></li>
</ul>
