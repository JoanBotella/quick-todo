<?php
declare(strict_types=1);

namespace quickTodo\page\todoItemForm\service\widget\headerContent;

use quickTodo\service\templater\Templater;

final class TodoItemFormHeaderContentWidget
{

	public static function render(array $context):string
	{
		return Templater::render(
			__DIR__.'/template.php',
			$context
		);
	}

}