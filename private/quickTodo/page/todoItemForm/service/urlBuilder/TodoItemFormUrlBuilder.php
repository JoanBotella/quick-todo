<?php
declare(strict_types=1);

namespace quickTodo\page\todoItemForm\service\urlBuilder;

final class TodoItemFormUrlBuilder
{

	public static function buildByTodoListId(int $todoListId):string
	{
		return self::build().'?todo_list_id='.$todoListId;
	}

		private static function build():string
		{
			return 'todoItemForm';
		}

	public static function buildByTodoItemId(int $todoItemId):string
	{
		return self::build().'?todo_item_id='.$todoItemId;
	}

}