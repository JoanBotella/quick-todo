<?php
declare(strict_types=1);

namespace quickTodo\page\todoItemForm\library\controller;

use quickTodo\library\controller\ControllerAbs;
use quickTodo\page\processTodoItemInsert\service\urlBuilder\ProcessTodoItemInsertUrlBuilder;
use quickTodo\page\processTodoItemUpdate\service\urlBuilder\ProcessTodoItemUpdateUrlBuilder;
use quickTodo\page\todoItemForm\service\widget\headerContent\TodoItemFormHeaderContentWidget;
use quickTodo\page\todoItemForm\service\widget\mainContent\TodoItemFormMainContentWidget;
use quickTodo\page\todoItemForm\service\widget\navContent\TodoItemFormNavContentWidget;
use quickTodo\page\todoList\service\urlBuilder\TodoListUrlBuilder;
use quickTodo\service\repository\todoItem\TodoItemRepository;
use quickTodo\service\repository\todoList\TodoListRepository;
use quickTodo\service\widget\layout\LayoutWidget;

final class TodoItemFormController extends ControllerAbs
{

	private int $todoListId;

	private int $todoItemId;

	protected function setupAndValidateInput():void
	{
		if (isset($_GET['todo_list_id']))
		{
			$this->todoListId = (int) $_GET['todo_list_id'];
		}
		else if (isset($_GET['todo_item_id']))
		{
			$this->todoItemId = (int) $_GET['todo_item_id'];
		}
	}

	protected function respond():void
	{
		$context = [
			'languageCode' => 'en',
			'pageCode' => 'todo_item_form',
			'pageTitle' => 'ToDo List Form',
		];

		if (isset($this->todoListId))
		{
			$context['todoList'] = TodoListRepository::selectById($this->todoListId);
			$context['todoListUrl'] = TodoListUrlBuilder::build($this->todoListId);
			$context['formAction'] = ProcessTodoItemInsertUrlBuilder::build();
		}
		else if (isset($this->todoItemId))
		{
			$context['todoItem'] = TodoItemRepository::selectById($this->todoItemId);
			$context['todoListUrl'] = TodoListUrlBuilder::build((int) $context['todoItem']['todo_list_id']);
			$context['formAction'] = ProcessTodoItemUpdateUrlBuilder::build();
		}

		$context['headerContent'] = TodoItemFormHeaderContentWidget::render($context);

		$context['navContent'] = TodoItemFormNavContentWidget::render($context);

		$context['mainContent'] = TodoItemFormMainContentWidget::render($context);

		echo LayoutWidget::render($context);
	}

}