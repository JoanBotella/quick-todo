<?php
declare(strict_types=1);

namespace quickTodo\page\processTodoItemDelete\library\controller;

use quickTodo\library\controller\ControllerAbs;
use quickTodo\page\todoList\service\urlBuilder\TodoListUrlBuilder;
use quickTodo\service\repository\todoItem\TodoItemRepository;

final class ProcessTodoItemDeleteController extends ControllerAbs
{

	private int $todoItemId;

	private int $todoListId;

	protected function setupAndValidateInput():void
	{
		$this->todoItemId = (int) $_GET['todo_item_id'];
	}

	protected function run():void
	{
		$todoItem = TodoItemRepository::selectById(
			$this->todoItemId
		);

		$this->todoListId = (int) $todoItem['todo_list_id'];

		TodoItemRepository::deleteById(
			$this->todoItemId
		);
	}

	protected function respond():void
	{
		header('location: '.TodoListUrlBuilder::build($this->todoListId));
	}

}