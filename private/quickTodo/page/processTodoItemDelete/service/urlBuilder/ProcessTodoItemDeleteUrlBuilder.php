<?php
declare(strict_types=1);

namespace quickTodo\page\processTodoItemDelete\service\urlBuilder;

final class ProcessTodoItemDeleteUrlBuilder
{

	public static function buildByTodoItemId(int $todoItemId):string
	{
		return 'processTodoItemDelete?todo_item_id='.$todoItemId;
	}

}