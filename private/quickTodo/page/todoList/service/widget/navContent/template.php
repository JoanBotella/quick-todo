<?php
declare(strict_types=1);

use quickTodo\page\home\service\urlBuilder\HomeUrlBuilder;
use quickTodo\page\todoItemForm\service\urlBuilder\TodoItemFormUrlBuilder;
use quickTodo\page\todoListForm\service\urlBuilder\TodoListFormUrlBuilder;

?>

<ul class="widget-todo_list-nav_content">
	<li class="back"><a class="fas fa-caret-square-left" href="<?= HomeUrlBuilder::build() ?>"></a></li>
	<li class="edit"><a class="fas fa-pen-square" href="<?= TodoListFormUrlBuilder::buildByTodoListId((int) $context['todoList']['id']) ?>"></a></li>
	<li class="add"><a class="fas fa-plus-square" href="<?= TodoItemFormUrlBuilder::buildByTodoListId((int) $context['todoList']['id']) ?>"></a></li>
</ul>
