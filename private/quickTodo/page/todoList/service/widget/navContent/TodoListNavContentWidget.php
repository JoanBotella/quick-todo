<?php
declare(strict_types=1);

namespace quickTodo\page\todoList\service\widget\navContent;

use quickTodo\service\templater\Templater;

final class TodoListNavContentWidget
{

	public static function render(array $context):string
	{
		return Templater::render(
			__DIR__.'/template.php',
			$context
		);
	}

}