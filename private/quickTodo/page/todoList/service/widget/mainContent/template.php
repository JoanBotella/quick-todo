<?php
declare(strict_types=1);

use quickTodo\page\processTodoItemDelete\service\urlBuilder\ProcessTodoItemDeleteUrlBuilder;
use quickTodo\page\todoItemForm\service\urlBuilder\TodoItemFormUrlBuilder;

?>

<ul class="widget-todo_list-main_content" data-hook="list">
	<?php foreach ($context['todoItems'] as $todoItem): ?>
	<li<?php if ($todoItem['checked'] == '1'): ?> class="checked"<?php endif ?> data-todo-item-id="<?= $todoItem['id'] ?>">
		<a class="delete fas fa-minus-square" href="<?= ProcessTodoItemDeleteUrlBuilder::buildByTodoItemId((int) $todoItem['id']) ?>" data-hook="delete_link"></a>
		<span class="name" data-hook="todo_item_name"><?= $todoItem['caption'] ?></span>
		<a class="edit fas fa-pen-square" href="<?= TodoItemFormUrlBuilder::buildByTodoItemId((int) $todoItem['id']) ?>"></a>
	</li>
	<?php endforeach ?>
</ul>
