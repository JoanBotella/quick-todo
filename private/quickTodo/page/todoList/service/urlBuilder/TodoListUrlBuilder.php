<?php
declare(strict_types=1);

namespace quickTodo\page\todoList\service\urlBuilder;

final class TodoListUrlBuilder
{

	public static function build(int $todoListId):string
	{
		return 'todoList?todo_list_id='.$todoListId;
	}

}