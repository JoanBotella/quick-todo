<?php
declare(strict_types=1);

namespace quickTodo\page\todoList\library\controller;

use quickTodo\library\controller\ControllerAbs;
use quickTodo\page\todoList\service\widget\headerContent\TodoListHeaderContentWidget;
use quickTodo\page\todoList\service\widget\mainContent\TodoListMainContentWidget;
use quickTodo\page\todoList\service\widget\navContent\TodoListNavContentWidget;
use quickTodo\service\repository\todoItem\TodoItemRepository;
use quickTodo\service\repository\todoList\TodoListRepository;
use quickTodo\service\widget\layout\LayoutWidget;

final class TodoListController extends ControllerAbs
{

	private int $todoListId;

	protected function setupAndValidateInput():void
	{
		$this->todoListId = (int) $_GET['todo_list_id'];
	}

	protected function respond():void
	{
		$context = [
			'languageCode' => 'en',
			'pageCode' => 'todo_list',
			'pageTitle' => 'ToDo List',

			'todoList' => TodoListRepository::selectById($this->todoListId),
			'todoItems' => TodoItemRepository::selectByTodoListId($this->todoListId),
		];

		$context['headerContent'] = TodoListHeaderContentWidget::render($context);

		$context['navContent'] = TodoListNavContentWidget::render($context);

		$context['mainContent'] = TodoListMainContentWidget::render($context);

		echo LayoutWidget::render($context);
	}

}