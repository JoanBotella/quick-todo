
pageSetuppers['todo_list'] = function ()
{
	const confirmDeletion = function (event)
	{
		if (!confirm('Are you sure?'))
		{
			event.preventDefault();
		}
	}

	const addConfirmToDeleteLinks = function ()
	{
		const deleteLinks = document.querySelectorAll('[data-hook="delete_link"]');
		for (let i = 0; i < deleteLinks.length; i++)
		{
			deleteLinks[i].addEventListener('click', confirmDeletion);
		}
	}

	const sortTodoItems = function ()
	{
		const
			list = document.querySelector('[data-hook="list"]'),
			items = list.querySelectorAll('li')
		;

		let
			checkedItems = [],
			uncheckedItems = []
		;

		for (let i = 0; i < items.length; i++)
		{
			if (items[i].classList.contains('checked'))
			{
				checkedItems.push(items[i]);
			}
			else
			{
				uncheckedItems.push(items[i]);
			}
		}

		const sortAlphabetically = function (elementA, elementB)
		{
			const
				a = elementA.querySelector('[data-hook="todo_item_name"]').textContent,
				b = elementB.querySelector('[data-hook="todo_item_name"]').textContent
			;
			if(a < b)
			{
				return -1;
			}
			if(a > b)
			{
				return 1;
			}
			return 0;
		}

		checkedItems.sort(sortAlphabetically);

		uncheckedItems.sort(sortAlphabetically);

		list.innerHtml = '';

		for (let i = 0; i < uncheckedItems.length; i++)
		{
			list.appendChild(uncheckedItems[i]);
		}

		for (let i = 0; i < checkedItems.length; i++)
		{
			list.appendChild(checkedItems[i]);
		}
	}

	const checkOrUncheckTodoItem = function (event)
	{
		const
			name = event.target,
			li = name.parentElement,
			todoItemId = li.getAttribute('data-todo-item-id'),
			checked =
				li.classList.contains('checked')
					? 'false'
					: 'true'
			,
			url = 'apiUpdateTodoItemChecked?todo_item_id=' + todoItemId + '&checked=' + checked
		;
		fetch(url)
			.then(
				function ()
				{
					li.classList.toggle('checked');
					sortTodoItems();
				}
			)
		;
	}

	const addCheckOrUncheckTodoItemToNames = function ()
	{
		const names = document.querySelectorAll('[data-hook="todo_item_name"]');

		for (let i = 0; i < names.length; i++)
		{
			names[i].addEventListener('click', checkOrUncheckTodoItem);
		}
	}

	addConfirmToDeleteLinks();
	addCheckOrUncheckTodoItemToNames();
}