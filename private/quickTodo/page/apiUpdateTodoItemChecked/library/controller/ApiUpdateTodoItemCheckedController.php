<?php
declare(strict_types=1);

namespace quickTodo\page\apiUpdateTodoItemChecked\library\controller;

use quickTodo\library\controller\ControllerAbs;
use quickTodo\service\repository\todoItem\TodoItemRepository;

final class ApiUpdateTodoItemCheckedController extends ControllerAbs
{

	private int $todoItemId;

	private bool $checked;

	private array $todoItem;

	protected function setupAndValidateInput():void
	{
		$this->todoItemId = (int) $_GET['todo_item_id'];

		if ($_GET['checked'] === 'true')
		{
			$this->checked = true;
		}
		else if ($_GET['checked'] === 'false')
		{
			$this->checked = false;
		}
	}

	protected function run():void
	{
		$this->todoItem = TodoItemRepository::selectById(
			$this->todoItemId
		);

		TodoItemRepository::updateById(
			$this->todoItemId,
			$this->todoItem['caption'],
			$this->checked
		);
	}

}