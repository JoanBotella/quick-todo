<?php
declare(strict_types=1);

namespace quickTodo\page\apiUpdateTodoItemChecked\service\urlBuilder;

final class ApiUpdateTodoItemCheckedUrlBuilder
{

	public static function build():string
	{
		return 'apiUpdateTodoItemChecked';
	}

}