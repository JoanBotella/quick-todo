<?php
declare(strict_types=1);

namespace quickTodo\page\todoListForm\library\controller;

use quickTodo\library\controller\ControllerAbs;
use quickTodo\page\home\service\urlBuilder\HomeUrlBuilder;
use quickTodo\page\processTodoListInsert\service\urlBuilder\ProcessTodoListInsertUrlBuilder;
use quickTodo\page\processTodoListUpdate\service\urlBuilder\ProcessTodoListUpdateUrlBuilder;
use quickTodo\page\todoList\service\urlBuilder\TodoListUrlBuilder;
use quickTodo\page\todoListForm\service\widget\headerContent\TodoListFormHeaderContentWidget;
use quickTodo\page\todoListForm\service\widget\mainContent\TodoListFormMainContentWidget;
use quickTodo\page\todoListForm\service\widget\navContent\TodoListFormNavContentWidget;
use quickTodo\service\repository\todoList\TodoListRepository;
use quickTodo\service\widget\layout\LayoutWidget;

final class TodoListFormController extends ControllerAbs
{

	private array $todoList;

	protected function setupAndValidateInput():void
	{
		if (isset($_GET['todo_list_id']))
		{
			$this->todoList = TodoListRepository::selectById((int) $_GET['todo_list_id']);
		}
	}

	protected function respond():void
	{
		$context = [
			'languageCode' => 'en',
			'pageCode' => 'todo_list_form',
			'pageTitle' => 'ToDo List Form',
		];

		if (isset($this->todoList))
		{
			$context['todoList'] = $this->todoList;
			$context['backUrl'] = TodoListUrlBuilder::build((int) $this->todoList['id']);
			$context['formAction'] = ProcessTodoListUpdateUrlBuilder::build();
		}
		else
		{
			$context['backUrl'] = HomeUrlBuilder::build();
			$context['formAction'] = ProcessTodoListInsertUrlBuilder::build();
		}

		$context['headerContent'] = TodoListFormHeaderContentWidget::render($context);

		$context['navContent'] = TodoListFormNavContentWidget::render($context);

		$context['mainContent'] = TodoListFormMainContentWidget::render($context);

		echo LayoutWidget::render($context);
	}

}