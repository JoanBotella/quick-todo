<?php
declare(strict_types=1);

?>

<form class="widget-todo_list_form-main_content" action="<?= $context['formAction'] ?>" method="POST">

	<?php if (isset($context['todoList'])): ?>
	<input type="hidden" name="todo_list_id" value="<?= $context['todoList']['id'] ?>" />
	<?php endif ?>

	<label>
		<span>Name</span>
		<input name="name"<?php if (isset($context['todoList'])): ?> value="<?= $context['todoList']['name'] ?>"<?php endif ?> autofocus="autofocus" />
	</label>

	<button>Save</button>

</form>
