<?php
declare(strict_types=1);

namespace quickTodo\page\todoListForm\service\widget\headerContent;

use quickTodo\service\templater\Templater;

final class TodoListFormHeaderContentWidget
{

	public static function render(array $context):string
	{
		return Templater::render(
			__DIR__.'/template.php',
			$context
		);
	}

}