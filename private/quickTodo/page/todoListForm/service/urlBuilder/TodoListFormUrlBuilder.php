<?php
declare(strict_types=1);

namespace quickTodo\page\todoListForm\service\urlBuilder;

final class TodoListFormUrlBuilder
{

	public static function build():string
	{
		return 'todoListForm';
	}

	public static function buildByTodoListId(int $todoListId):string
	{
		return self::build().'?todo_list_id='.$todoListId;
	}

}