<?php
declare(strict_types=1);

namespace quickTodo\page\processTodoListUpdate\service\urlBuilder;

final class ProcessTodoListUpdateUrlBuilder
{

	public static function build():string
	{
		return 'processTodoListUpdate';
	}

}