<?php
declare(strict_types=1);

namespace quickTodo\page\processTodoListUpdate\library\controller;

use quickTodo\library\controller\ControllerAbs;
use quickTodo\page\todoList\service\urlBuilder\TodoListUrlBuilder;
use quickTodo\service\repository\todoList\TodoListRepository;

final class ProcessTodoListUpdateController extends ControllerAbs
{

	private int $todoListId;

	private string $name;

	protected function setupAndValidateInput():void
	{
		$this->todoListId = (int) $_POST['todo_list_id'];
		$this->name = $_POST['name'];
	}

	protected function run():void
	{
		TodoListRepository::updateById(
			$this->todoListId,
			$this->name
		);
	}

	protected function respond():void
	{
		header('location: '.TodoListUrlBuilder::build($this->todoListId));
	}

}