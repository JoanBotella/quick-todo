<?php
declare(strict_types=1);

namespace quickTodo\page\processTodoListInsert\library\controller;

use quickTodo\library\controller\ControllerAbs;
use quickTodo\page\home\service\urlBuilder\HomeUrlBuilder;
use quickTodo\service\repository\todoList\TodoListRepository;

final class ProcessTodoListInsertController extends ControllerAbs
{

	private string $name;

	protected function setupAndValidateInput():void
	{
		$this->name = $_POST['name'];

	}

	protected function run():void
	{
		TodoListRepository::insert(
			$this->name
		);
	}

	protected function respond():void
	{
		header('location: '.HomeUrlBuilder::build());
	}

}