<?php
declare(strict_types=1);

namespace quickTodo\page\processTodoListInsert\service\urlBuilder;

final class ProcessTodoListInsertUrlBuilder
{

	public static function build():string
	{
		return 'processTodoListInsert';
	}

}