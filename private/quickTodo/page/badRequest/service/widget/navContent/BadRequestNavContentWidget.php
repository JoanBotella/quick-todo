<?php
declare(strict_types=1);

namespace quickTodo\page\badRequest\service\widget\navContent;

use quickTodo\service\templater\Templater;

final class BadRequestNavContentWidget
{

	public static function render(array $context):string
	{
		return Templater::render(
			__DIR__.'/template.php',
			$context
		);
	}

}