<?php
declare(strict_types=1);

namespace quickTodo\page\badRequest\library\controller;

use quickTodo\library\controller\ControllerAbs;
use quickTodo\page\badRequest\service\widget\headerContent\BadRequestHeaderContentWidget;
use quickTodo\page\badRequest\service\widget\mainContent\BadRequestMainContentWidget;
use quickTodo\page\badRequest\service\widget\navContent\BadRequestNavContentWidget;
use quickTodo\service\widget\layout\LayoutWidget;

final class BadRequestController extends ControllerAbs
{

	protected function respond():void
	{
		http_response_code(400);

		$context = [
			'languageCode' => 'en',
			'pageCode' => 'bad_request',
			'pageTitle' => 'Bad Request',
		];

		$context['headerContent'] = BadRequestHeaderContentWidget::render($context);

		$context['navContent'] = BadRequestNavContentWidget::render($context);

		$context['mainContent'] = BadRequestMainContentWidget::render($context);

		echo LayoutWidget::render($context);
	}

}