<?php
declare(strict_types=1);

namespace quickTodo\page\processTodoItemUpdate\service\urlBuilder;

final class ProcessTodoItemUpdateUrlBuilder
{

	public static function build():string
	{
		return 'processTodoItemUpdate';
	}

}