<?php
declare(strict_types=1);

namespace quickTodo\page\processTodoItemUpdate\library\controller;

use quickTodo\library\controller\ControllerAbs;
use quickTodo\page\todoList\service\urlBuilder\TodoListUrlBuilder;
use quickTodo\service\repository\todoItem\TodoItemRepository;

final class ProcessTodoItemUpdateController extends ControllerAbs
{

	private int $todoItemId;

	private string $caption;

	private bool $checked;

	private array $todoItem;

	protected function setupAndValidateInput():void
	{
		$this->todoItemId = (int) $_POST['todo_item_id'];
		$this->caption = $_POST['caption'];
		$this->checked = isset($_POST['checked']);
	}

	protected function run():void
	{
		$this->todoItem = TodoItemRepository::selectById(
			$this->todoItemId
		);

		TodoItemRepository::updateById(
			$this->todoItemId,
			$this->caption,
			$this->checked
		);
	}

	protected function respond():void
	{
		header('location: '.TodoListUrlBuilder::build((int) $this->todoItem['todo_list_id']));
	}

}