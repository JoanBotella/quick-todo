<?php
declare(strict_types=1);

namespace quickTodo\library\controller;

interface ControllerItf
{

	public function action():void;

}