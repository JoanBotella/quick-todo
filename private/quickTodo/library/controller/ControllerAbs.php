<?php
declare(strict_types=1);

namespace quickTodo\library\controller;

use quickTodo\library\controller\ControllerItf;
use quickTodo\page\badRequest\library\controller\BadRequestController;

abstract class ControllerAbs implements ControllerItf
{

	protected bool $isInputValid;

	public function action():void
	{
		$this->isInputValid = true;

		$this->setupAndValidateInput();

		if (!$this->isInputValid)
		{
			$badRequestController = new BadRequestController();
			$badRequestController->action();
			return;
		}

		$this->run();

		$this->respond();
	}

		protected function setupAndValidateInput():void
		{
		}

		protected function run():void
		{
		}

		protected function respond():void
		{
		}

}