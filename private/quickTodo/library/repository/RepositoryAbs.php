<?php
declare(strict_types=1);

namespace quickTodo\library\repository;

abstract class RepositoryAbs
{

	protected static function boolToInteger(bool $value):int
	{
		return
			$value
				? 1
				: 0
		;
	}

}