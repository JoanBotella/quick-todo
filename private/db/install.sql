
CREATE TABLE IF NOT EXISTS
	todo_list
	(
		id INTEGER PRIMARY KEY,
		name TEXT
	)
;

CREATE TABLE IF NOT EXISTS
	todo_item
	(
		id INTEGER PRIMARY KEY,
		todo_list_id INTEGER,
		caption TEXT,
		checked INTEGER
	)
;